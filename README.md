# GovGeeks T-Shirts

Artwork for #GovGeeks T-Shirts

* Read the blog! https://shkspr.mobi/blog/2019/03/govgeeks-t-shirts/
* Buy the t-shirts! https://shop.spreadshirt.co.uk/govgeeks/

## Copyright and Copyleft

* Make Things Open, OGLv3 from Government Digital Service (Henry Hadlow and Ben Terrett)
* Users First, OGL v3 Government Digital Service (Ralph Hawkins and Russell Davies)
* Minimum Viable Bureaucracy, by [Matt Jukes](https://twitter.com/jukesie)
* Bold Blogging by [Jeanette Clement](http://www.clementgraphics.com/)
* NHS artwork Crown Copyright, redesigned by [Matt Edgar](https://mattedgar.com/)

Please remix and reuse!
